﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ChaosTheoryProject.Drawing.Shapes
{
    public abstract class TAbstractFilledShape : TAbstractShape
    {
        protected bool m_Filled = true;
        public bool Filled
        {
            get
            {
                return m_Filled;
            }

            set
            {
                m_Filled = value;
            }
        }

        protected Brush m_BrushStyle = Brushes.DarkGreen;
        public Brush BrushStyle
        {
            get
            {
                return m_BrushStyle;
            }

            set         
            {
                m_BrushStyle = value;
            }
        }
    }
}
