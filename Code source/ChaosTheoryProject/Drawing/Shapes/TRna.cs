﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ChaosTheoryProject.Drawing.Shapes
{
    public class TRna : TAbstractShape, IShape
    {
        private Rectangle m_Bounds = new Rectangle();
        private List<TNeuron> m_Entries = new List<TNeuron>();
        private List<TNeuron> m_Hidden = new List<TNeuron>();
        private List<TNeuron> m_Output = new List<TNeuron>();
        private static Font m_FontLabel = new Font("Courier New", 8);

        public TRna(int entry, int hidden, int output)
        {
            for (int i = 0; i < entry; i++)
                m_Entries.Add(new TNeuron("" + (i+1), 20));

            for (int i = 0; i < hidden; i++)
                m_Hidden.Add(new TNeuron("" + (i + 1), 20));

            for (int i = 0; i < output; i++)
                m_Output.Add(new TNeuron("" + (i + 1), 20));
        }

        public void SetBounds(int x, int y, int wdt, int hgt)
        {
            m_Bounds.X = x;
            m_Bounds.Y = y;
            m_Bounds.Width = wdt;
            m_Bounds.Height = hgt;
        }

        private void Refresh()
        {            
            int d = 1;
            if (m_Entries.Count > 0)
                d = m_Entries.Count;
            int yPos = m_Bounds.Y + m_Bounds.Height / (d+1);

            foreach (TNeuron entry in m_Entries)
            {
                entry.SetLocation(m_Bounds.X, yPos);
                yPos += m_Bounds.Height / (d+1);
            }
            
            d = 1;
            if (m_Hidden.Count > 0)
                d = m_Hidden.Count;
            yPos = m_Bounds.Y + m_Bounds.Height / (d + 1);

            foreach (TNeuron hidden in m_Hidden)
            {
                hidden.SetLocation(m_Bounds.X + m_Bounds.Width / 2, yPos);
                yPos += m_Bounds.Height / (d+1);
            }
            
            d = 1;
            if (m_Output.Count > 0)
                d = m_Output.Count;
            yPos = m_Bounds.Y + m_Bounds.Height / (d + 1);

            foreach (TNeuron output in m_Output)
            {
                output.SetLocation(m_Bounds.X + m_Bounds.Width, yPos);
                yPos += m_Bounds.Height / (d+1);
            }            
        }

        private Point pBuf = new Point();
        public void Draw(System.Drawing.Graphics g)
        {
            Refresh();
            
            //Traçage des connections
            foreach (TNeuron entry in m_Entries)
                foreach (TNeuron hidden in m_Hidden)
                {
                    Point p1 = entry.Location;
                    Point p2 = hidden.Location;
                    
                    pBuf.X = (p1.X + p2.X) / 2;
                    pBuf.Y = (p1.Y + p2.Y) / 2;

                    g.DrawLine(m_PenStyle, p1, p2);
                    g.DrawString("W" + hidden.Caption + entry.Caption, m_FontLabel, Brushes.White, pBuf);
                }

            foreach (TNeuron hidden in m_Hidden)
                foreach (TNeuron output in m_Output)
                {
                    Point p1 = hidden.Location;
                    Point p2 = output.Location;

                    pBuf.X = (p1.X + p2.X) / 2;
                    pBuf.Y = (p1.Y + p2.Y) / 2;

                    g.DrawLine(m_PenStyle, p1, p2);
                    g.DrawString("W" + output.Caption + hidden.Caption, m_FontLabel, Brushes.White, pBuf);
                }

            //Traçage des neurones
            foreach (TNeuron sh in m_Entries)
                sh.Draw(g);

            foreach (TNeuron sh in m_Hidden)
                sh.Draw(g);

            foreach (TNeuron sh in m_Output)
                sh.Draw(g);
        }
    }
}
