﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ChaosTheoryProject.Drawing.Shapes
{
    public class TCircle : TAbstractFilledShape, IShape
    {
        private Rectangle m_Bounds = new Rectangle();

        public TCircle():this(0)
        {
        }

        public TCircle(int radius):this(0, 0, radius)
        {
        }

        public TCircle(int x, int y, int radius)
        {
            m_Bounds.X = x - radius;
            m_Bounds.Y = y - radius;
            m_Bounds.Width = 2 * radius;
            m_Bounds.Height = 2 * radius;
        }

        public void Draw(System.Drawing.Graphics g)
        {
            if (m_Filled)
                g.DrawEllipse(m_PenStyle, m_Bounds);

            g.FillEllipse(m_BrushStyle, m_Bounds);
        }

        public void SetLocation(int x, int y)
        {
            m_Bounds.X = x - m_Bounds.Width/2;
            m_Bounds.Y = y - m_Bounds.Height/2;
        }

        private Point pBuf = new Point();
        public Point Location
        {
            get
            {
                pBuf.X = m_Bounds.X + m_Bounds.Width/2;
                pBuf.Y = m_Bounds.Y + m_Bounds.Width/2;

                return pBuf;
            }
        }
    }
}
