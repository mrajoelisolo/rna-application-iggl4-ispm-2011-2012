﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ChaosTheoryProject.Drawing.Shapes
{
    public class TRectangle : TAbstractFilledShape, IShape
    {
        private Rectangle m_Bounds = new Rectangle();

        public TRectangle(int x, int y, int wdt, int hgt)
        {
            m_Bounds.X = x;
            m_Bounds.Y = y;
            m_Bounds.Width = wdt;
            m_Bounds.Height = hgt;
        }

        public void Draw(Graphics g)
        {
            if (m_Filled)            
                g.FillRectangle(m_BrushStyle, m_Bounds);

            g.DrawRectangle(m_PenStyle, m_Bounds);
        }
    }
}
