﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ChaosTheoryProject.Drawing.Shapes
{
    public abstract class TAbstractShape
    {
        protected Pen m_PenStyle = Pens.Green;
        public Pen PenStyle
        {
            get
            {
                return m_PenStyle;
            }

            set
            {
                m_PenStyle = value;
            }
        }
    }
}
