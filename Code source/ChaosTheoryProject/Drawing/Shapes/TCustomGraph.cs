﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChaosTheoryProject.Drawing.Shapes;
using System.Drawing;
using System.Windows.Forms;
using ChaosTheoryProject.Datasource;

namespace ChaosTheoryProject.Core
{
    public class TCustomGraph : TAbstractShape, IShape
    {
        private Ucs m_Ucs;
        private Font m_DisplayFont = new Font("Courier New", 12);
        private RungeKutta4 m_Data;
        private int m_Count;
        private double xMin, xMax;
        private double yMin, yMax;

        public TCustomGraph(UserControl parent)
        {
            m_Ucs = new Ucs(parent);
            m_Ucs.SetOrigin(50, 200);
            m_Ucs.Scale = 10f;
            m_Ucs.PenStyle = Pens.White;
            m_Ucs.BrushStyle = Brushes.White;

            this.m_Count = 5000;
            this.m_Data = new RungeKutta4(m_Count);
            m_Data.Compute();
        }
 
        public void Draw(System.Drawing.Graphics g)
        {
            xMin = m_Data.GetZ(0); xMax = m_Data.GetZ(0);
            yMin = m_Data.GetX(0); yMax = m_Data.GetX(0);

            for (int i = 1; i < m_Count - 1; i++)
            {
                Point p = m_Ucs.Transform(m_Data.GetZ(i), m_Data.GetX(i));
                int x1 = p.X; int y1 = p.Y;

                p = m_Ucs.Transform(m_Data.GetZ(i + 1), m_Data.GetX(i + 1));
                int x2 = p.X; int y2 = p.Y;

                g.DrawLine(m_PenStyle, x1, y1, x2, y2);

                if (xMin > m_Data.GetZ(i)) xMin = m_Data.GetZ(i);
                if (xMax < m_Data.GetZ(i)) xMax = m_Data.GetZ(i);
                if (yMin > m_Data.GetZ(i)) yMin = m_Data.GetZ(i);
                if (yMax < m_Data.GetZ(i)) yMax = m_Data.GetZ(i);
            }

            m_Ucs.Draw(g);
        }

        public List<RSerie> GetListSerie()
        {
            List<RSerie> res = new List<RSerie>();

            int i = 0;
            foreach (double val in m_Data.GetXArray())
                res.Add(new RSerie(i++, val));

            return res;
        }

        public double[] GetArraySerie()
        {
            return m_Data.GetXArray();
        }
    }
}
