﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ChaosTheoryProject.Drawing.Shapes
{
    public class TNeuron : TAbstractFilledShape, IShape
    {
        private static Font m_FontLabel = new Font("Courier New", 12);
        private Rectangle m_Bounds = new Rectangle();
        private string m_Caption;
        private int m_Radius;

        public TNeuron(string caption, int radius)
        {
            m_Caption = caption;
            m_Radius = radius;
            m_Bounds.Width = 2 * radius;
            m_Bounds.Height = 2 * radius;
        }

        public void Draw(System.Drawing.Graphics g)
        {
            if (m_Filled)
                g.DrawEllipse(m_PenStyle, m_Bounds);

            g.FillEllipse(m_BrushStyle, m_Bounds);

            g.DrawString(m_Caption, m_FontLabel, Brushes.White, m_Bounds.X + 10, m_Bounds.Y + 10);
        }

        public void SetLocation(int x, int y)
        {
            m_Bounds.X = x - m_Radius;
            m_Bounds.Y = y - m_Radius;
        }

        private Point pBuf = new Point();
        public Point Location
        {
            get
            {
                pBuf.X = m_Bounds.X + m_Radius;
                pBuf.Y = m_Bounds.Y + m_Radius;

                return pBuf;
            }
        }

        public string Caption
        {
            get
            {
                return m_Caption;
            }

            set
            {
                m_Caption = value;
            }
        }
    }
}
