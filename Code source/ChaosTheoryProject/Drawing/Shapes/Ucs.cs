﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChaosTheoryProject.Drawing.Shapes;
using System.Windows.Forms;
using System.Drawing;

namespace ChaosTheoryProject.Core
{
    public class Ucs : TAbstractFilledShape, IShape
    {
        private UserControl m_Parent;
        private Point m_Origin = new Point();
        private double m_Scale = 1d;
        private Font m_DisplayFont = new Font("Courier New", 12);

        public Ucs(UserControl parent):this(parent, 0, 0) 
        { 
        }

        public Ucs(UserControl parent, int x, int y)
        {
            m_Parent = parent;
        }

        public void Draw(System.Drawing.Graphics g)
        {
            int wdt = m_Parent.Width;
            int hgt = m_Parent.Height;

            g.DrawLine(m_PenStyle, 0, hgt - m_Origin.Y, wdt, hgt - m_Origin.Y);
            g.DrawLine(m_PenStyle,  m_Origin.X, 0, m_Origin.X, hgt);
            g.DrawString("0", m_DisplayFont, m_BrushStyle, m_Origin.X - 12, hgt - m_Origin.Y + 5);
            int r = 4;
            g.DrawEllipse(m_PenStyle, m_Origin.X - r, hgt - m_Origin.Y - r, 2 * r, 2 * r);

            g.DrawString("Z axis", m_DisplayFont, m_BrushStyle, wdt - 70, hgt - m_Origin.Y + 14);
            g.DrawString("X axis", m_DisplayFont, m_BrushStyle, m_Origin.X + 10, 14);
        }

        public double Scale
        {
            get
            {
                return m_Scale;
            }

            set
            {
                m_Scale = value;
            }
        }

        public void SetOrigin(int x, int y)
        {
            m_Origin.X = x;
            m_Origin.Y = y;
        }

        private Point pBuf = new Point();
        public Point Transform(double x, double y)
        {
            pBuf.X = (int)(m_Origin.X + x * m_Scale);
            pBuf.Y = (int)(-m_Origin.Y + m_Parent.Bounds.Height - y * m_Scale);

            return pBuf;
        }
    }
}
