﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ChaosTheoryProject.Drawing.Shapes;

namespace ChaosTheoryProject.Drawing
{
    public partial class TCanvas : UserControl
    {
        private List<IShape> m_Shapes = new List<IShape>();

        public TCanvas()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            foreach (IShape shape in m_Shapes)
                shape.Draw(e.Graphics);
        }

        public void AddShape(IShape shape)
        {
            if (m_Shapes.Contains(shape)) return;

            m_Shapes.Add(shape);
            Refresh();
        }

        public void ClearShapes()
        {
            m_Shapes.Clear();
            Refresh();
        }
    }
}
