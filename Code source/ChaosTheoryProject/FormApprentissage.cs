﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ChaosTheoryProject.Core;

namespace ChaosTheoryProject
{
    public partial class FormApprentissage : Form
    {
        private Form m_Parent;
        private double[] m_X;
        private int m_Entries;
        private int m_Hidden;
        private bool m_CanContinue = false;

        public FormApprentissage(Form parent, int entries, int hidden, double[] X)
        {
            InitializeComponent();
            CenterToScreen();

            m_Parent = parent;
            m_X = X;
            m_Entries = entries;
            m_Hidden = hidden;
        }

        private void btnLearn_Click(object sender, EventArgs e)
        {
            Apprentissage.Instance.ApprentissageReseau(m_Hidden, m_Hidden, m_X);
            double[, ,] weights = Apprentissage.Instance.Reseau.GetW();
            AfficherPoids(weights);

            m_CanContinue = true;
        }

        public void AfficherPoids(double[, ,] W)
        {
            for (int i = 1; i <= m_Hidden; i++)
            {
                if (W[1, i, 3] == 0)
                    continue;
                else
                    listPoids.Items.Add("W[" + 1 + "," + i + "," + 3 + "]=" + W[1, i, 3]);

                for (int j = 1; j <= m_Entries; j++)
                    if (W[i, j, 2] == 0)
                        continue;
                    else
                        listPoids.Items.Add("W[" + i + "," + j + "," + 2 + "]=" + W[i, j, 2]);
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (!m_CanContinue)
            {
                MessageBox.Show("Impossible de continuer sans déterminer les poids des connexions");
                return;
            }

            FormPredictionUn f = new FormPredictionUn(this, m_Entries, m_Hidden, m_X);
            f.Show();
            this.Hide();
        }

        private void FormApprentissage_FormClosed(object sender, FormClosedEventArgs e)
        {
            m_Parent.Show();
        }
    }
}
