﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaosTheoryProject.Core
{
    public class Apprentissage
    {
        private static Apprentissage m_Instance = new Apprentissage();

        private Apprentissage() { }

        public static Apprentissage Instance
        {
            get
            {
                return m_Instance;
            }
        }

        private RNA m_Reseau;

        public void ApprentissageReseau(int entries, int hidden, double[] X)
        {
            int nbUEntree = entries;
            int nbUCachee = hidden;

            int indice = 0;

            /*1ere Periode*/
            m_Reseau = new RNA(nbUEntree, nbUCachee, X, indice);
            m_Reseau.Apprentissage();

            double erreur1 = m_Reseau.GetNMSE();

            double erreur2 = 0.0d;

            indice += 10;

            for (int periode = 2; periode <= 50; periode++)
            {
                m_Reseau.ReInit(nbUEntree, nbUCachee, X, indice);
                m_Reseau.Apprentissage();

                erreur2 = m_Reseau.GetNMSE();


                /*Test de sur-Apprentissage*/
                if (erreur2 > erreur1)
                {
                    break;
                }
                else
                {
                    erreur1 = erreur2;
                }

                indice += 10;
            }
        }

        public RNA Reseau
        {
            get
            {
                return m_Reseau;
            }
        }
    }
}
