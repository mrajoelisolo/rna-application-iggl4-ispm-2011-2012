﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaosTheoryProject.Core
{
    public class ArchitectureRNA
    {
        private static ArchitectureRNA m_Instance = new ArchitectureRNA();

        private ArchitectureRNA() { }

        public static ArchitectureRNA Instance
        {
            get
            {
                return m_Instance;
            }
        }

        public int GetNombreUniteEntree(double[] X)
        {
            //Takens t = new Takens(X, 100);

            //return t.GetNombreUniteCoucheEntree();
            return 2;
        }

        public int GetNombreUniteCachee(double[] X)
        {
            int nombreUniteEntree = GetNombreUniteEntree(X);

            int periode = 1;
            int indice = 0;

            /*Tableau Stockant les NMSE*/
            double[] NMSE = new double[51];

            /*Calcul des NMSE pour chaque période*/
            for (int nombreUniteCachee = 1; nombreUniteCachee <= 5; nombreUniteCachee++)
            {
                RNA retro = new RNA(nombreUniteEntree, nombreUniteCachee, X, indice);
                retro.Apprentissage();

                NMSE[periode] = retro.GetNMSE();

                indice += 10;
                periode++;
            }

            /*Recherche du NMSE minimum*/
            int nombreUniteCache = 1;
            double min = NMSE[1];
            for (int i = 1; i < NMSE.Length; i++)
            {
                if (NMSE[i] < min)
                {
                    min = NMSE[i];
                    nombreUniteCache = i;
                }
            }

            return nombreUniteEntree;
        }
    }
}
