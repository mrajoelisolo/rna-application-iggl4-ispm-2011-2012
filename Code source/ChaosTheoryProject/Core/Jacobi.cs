﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaosTheoryProject.Core
{
    class Jacobi
    {

        /*Constante Maximum Itération*/
        private const int ITERATION_MAX = 500;

        /*Attributs*/
        private Matrix A;
        private Matrix Ai;
        private Matrix P;
        private Matrix tP;

        /*Constructeur*/
        public Jacobi(Matrix M)
        {
            this.A = new Matrix(M.Dimension);
            this.Ai = new Matrix(M.Dimension);
            this.P = new Matrix(M.Dimension);
            this.tP = new Matrix(M.Dimension);

            this.A = M.Copy();
        }

        /*Diagonalisation de la matrice Ai*/
        private Matrix Diagonalisation()
        {
            /*Indice ligindices et colindices*/
            int[] indices = new int[2];

            /*Initialisation de P en Matrice Unitaire*/
            P = Matrix.Unite(P.Dimension);

            /*Recherche des indices j et k de la valeur indicesimum de la matrice Ai*/
            indices = Ai.GetIndexMaximumValue();

            double c = 0;
            double b = 0;

            /*Debut : Construction de P*/

            if (Ai[indices[0], indices[0]] == Ai[indices[1], indices[1]])
            {
                P[indices[0], indices[0]] = Math.Sqrt(2) / 2;
                P[indices[1], indices[1]] = Math.Sqrt(2) / 2;

                P[indices[1], indices[0]] = -Math.Sqrt(2) / 2;
                P[indices[0], indices[1]] = Math.Sqrt(2) / 2;
            }
            else
            {
                b = Ai[indices[0], indices[0]] - Ai[indices[1], indices[1]];

                if (Math.Sign(b) == 0)
                {
                    c = 2 * Ai[indices[0], indices[1]] * -1;
                }
                else
                {
                    c = 2 * Ai[indices[0], indices[1]];
                }

                b = Math.Abs(b);

                P[indices[0], indices[0]] = Math.Sqrt(0.5 * (1 + (b / Math.Sqrt(c * c + b * b))));
                P[indices[1], indices[1]] = Math.Sqrt(0.5 * (1 + (b / Math.Sqrt(c * c + b * b))));

                P[indices[1], indices[0]] = -(c / (2 * P[indices[0], indices[0]] * Math.Sqrt(c * c + b * b)));
                P[indices[0], indices[1]] = c / (2 * P[indices[0], indices[0]] * Math.Sqrt(c * c + b * b));
            }
            /*Fin : Construction de P*/

            /*tp est la Transposee de la Matrice P*/
            tP = P.Transpose();

            /* Ai=P*Ai*tP */

            Matrix Tmp = new Matrix(A.Dimension);
            Tmp = P * Ai;

            Matrix Jacobik = new Matrix(A.Dimension);
            Jacobik = Tmp * tP;

            return Jacobik;
        }

        /*Retourne les valeurs propres de la matrice A par ordre décroissant*/
        public double[] ValeursPropres()
        {
            double[] valeurPropre = new double[A.Dimension];
            Ai = A.Copy();

            double eps = Math.Abs(Ai.SommeElementExtraDiagonaux());
            int l = 0;

            /*Diagonalisation jusqu'à l->infini*/
            while (l < ITERATION_MAX || eps > 1e-8)
            {
                Ai = this.Diagonalisation();

                eps = Math.Abs(Ai.SommeElementExtraDiagonaux());
                l++;
            }

            /*Les elements de la diagonale de la Matrice A* sont es valeurs propres de A*/
            for (int i = 0; i < Ai.Dimension; i++)
            {
                valeurPropre[i] = UtilitaireNombre.Instance.FormatDouble(Ai[i, i]);
            }

            /*Trie par ordre decroissant des valeurs propres*/
            return trierDecroissant(valeurPropre);
        }

        /*Trie décroissaant d'un tableau ou d'une liste*/
        private double[] trierDecroissant(double[] liste)
        {
            for (int i = 0; i < liste.Length; i++)
            {
                for (int j = i; j < liste.Length; j++)
                {
                    if (liste[j] >= liste[i])
                    {
                        double tmp = liste[j];
                        liste[j] = liste[i];
                        liste[i] = tmp;
                    }
                }
            }

            return liste;
        }
    }
}
