﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaosTheoryProject.Core
{

    class Matrix
    {

        /***********************************************************************
        *                                                                      *
        *                                                                      *
        *                      Attributs et propriété                          *
        *                                                                      *
        *                                                                      *
        * **********************************************************************/

        private double[,] coefficient;
        private int dimension;

        /*Propriété Dimension de la classe Matrix*/
        public int Dimension
        {
            get
            {
                return this.dimension;
            }
            set
            {
                dimension = value;
            }
        }


        /***********************************************************************
        *                                                                      *
        *                                                                      *
        *                     Constructeurs                                    *
        *                                                                      *
        *                                                                      *
        * **********************************************************************/

        public Matrix(int dim)
        {
            this.dimension = dim;
            this.coefficient = new double[dim, dim];
            this.Init();
        }

        public Matrix(double[,] A)
        {
            this.dimension = (int)Math.Sqrt(A.Length);
            this.coefficient = new double[this.dimension, this.dimension];

            for (int i = 0; i < this.dimension; i++)
            {
                for (int j = 0; j < this.dimension; j++)
                {
                    this.coefficient[i, j] = A[i, j];
                }
            }
        }

        /***********************************************************************
        *                                                                      *
        *                                                                      *
        *                    Overrides et indexeurs                            *
        *                                                                      *
        *                                                                      *
        * **********************************************************************/

        /*Indexeurs*/
        public double this[int lig, int col]
        {
            get
            {
                if (lig < 0 || col < 0 || lig >= this.dimension || col >= this.dimension)
                {
                    Console.Error.Write("Index Hors Limites");
                    return 0;
                }
                else
                {
                    return this.coefficient[lig, col];
                }
            }
            set
            {
                if (lig < 0 || col < 0 || lig >= this.dimension || col >= this.dimension)
                {
                    Console.Error.Write("Index Hors Limites");
                    this.coefficient[lig, col] = 0;
                }
                else
                {
                    this.coefficient[lig, col] = value;
                }
            }
        }

        /*Overrride des opérateurs*/

        /*********************************Addition(+)*************************************/
        public static Matrix operator +(Matrix A, Matrix B)
        {
            if (A.Dimension != B.Dimension)
            {
                Console.Error.WriteLine("Les dimensions des deux matrices ne sont pas le même");
                return new Matrix(A.Dimension);
            }
            else
            {
                Matrix C = new Matrix(A.Dimension);

                for (int i = 0; i < A.Dimension; i++)
                {
                    for (int j = 0; j < A.Dimension; j++)
                    {
                        C[i, j] = A[i, j] + B[i, j];
                    }
                }

                return C;
            }
        }

        /**************************************Soustraction(-)*******************************/
        public static Matrix operator -(Matrix A, Matrix B)
        {
            if (A.Dimension != B.Dimension)
            {
                Console.Error.WriteLine("Les dimensions des deux matrices ne sont pas le même");
                return new Matrix(A.Dimension);
            }
            else
            {
                Matrix C = new Matrix(A.Dimension);

                for (int i = 0; i < A.Dimension; i++)
                {
                    for (int j = 0; j < A.Dimension; j++)
                    {
                        C[i, j] = A[i, j] - B[i, j];
                    }
                }

                return C;
            }
        }

        /**************************************Multiplication(*)******************************/
        public static Matrix operator *(Matrix A, Matrix B)
        {
            if (A.Dimension != B.Dimension)
            {
                Console.Error.WriteLine("Les dimensions des deux matrices ne sont pas le même");
                return new Matrix(A.Dimension);
            }
            else
            {
                Matrix C = new Matrix(A.Dimension);

                for (int i = 0; i < A.Dimension; i++)
                {
                    for (int j = 0; j < A.Dimension; j++)
                    {
                        C[i, j] = 0;
                        for (int k = 0; k < A.Dimension; k++)
                        {
                            C[i, j] += A[i, k] * B[k, j];
                        }
                    }
                }
                return C;
            }
        }

        /************************************************************************
         *                                                                      *
         *                                                                      *
         *                      Methodes publiques                              *
         *                                                                      *
         *                                                                      *
         * **********************************************************************/

        /*Initialisation de la Matrice*/
        public void Init()
        {
            for (int i = 0; i < this.dimension; i++)
            {
                for (int j = 0; j < this.dimension; j++)
                {
                    this.coefficient[i, j] = 0;
                }
            }
        }

        /*transforme la Matrice en Matrice unité*/
        public static Matrix Unite(int dim)
        {
            Matrix I = new Matrix(dim);

            for (int i = 0; i < dim; i++)
            {
                for (int j = 0; j < dim; j++)
                {
                    if (i == j)
                    {
                        I[i, j] = 1;
                    }
                    else
                    {
                        I[i, j] = 0;
                    }
                }
            }

            return I;
        }

        /*copie la matrice en cours dans une autre*/
        public Matrix Copy()
        {
            Matrix X = new Matrix(this.dimension);
            for (int i = 0; i < this.dimension; i++)
            {
                for (int j = 0; j < this.dimension; j++)
                {
                    X[i, j] = this.coefficient[i, j];
                }
            }

            return X;
        }

        /*retourne la transposee de la matrice*/
        public Matrix Transpose()
        {
            Matrix X = new Matrix(this.dimension);
            for (int i = 0; i < this.dimension; i++)
            {
                for (int j = 0; j < this.dimension; j++)
                {
                    X[i, j] = this.coefficient[j, i];
                }
            }

            return X;
        }

        /*Affiche les coefficient de la matrice dans une console*/
        public void Affiche()
        {
            for (int i = 0; i < this.dimension; i++)
            {
                for (int j = 0; j < this.dimension; j++)
                {
                    Console.Out.Write(this.coefficient[i, j] + "\t");
                }
                Console.Out.WriteLine("\n\n");
            }
        }

        /*fonction effectuant la tache suivante : Si M[i,j]<1e-15 alors on le considere comme egale a 0*/
        public void Perform()
        {
            for (int i = 0; i < this.dimension; i++)
            {
                for (int j = 0; j < this.dimension; j++)
                {
                    if (this.coefficient[i, j] < 1e-15)
                    {
                        this.coefficient[i, j] = 0.0;
                    }
                }
            }
        }

        /*Retourne les indices(lig, col) de la plus grande valeur dans la matrice*/
        public int[] GetIndexMaximumValue()
        {
            double maximum = 0;

            int[] indexTab = new int[2];

            for (int i = 0; i < this.dimension; i++)
            {
                for (int j = i + 1; j < this.dimension; j++)
                {
                    if (i < j)
                    {
                        if ((this.coefficient[i, j] != 0) && (Math.Abs(this.coefficient[i, j]) > maximum))
                        {
                            maximum = Math.Abs(Convert.ToDouble(this.coefficient[i, j].ToString("0.00000000")));
                            indexTab[0] = i;
                            indexTab[1] = j;
                        }
                    }
                }
            }

            return indexTab;
        }

        /*retourne les valeurs propres de la matrices algo de Jacobi*/
        public double[] GetValeursPropres()
        {
            Jacobi jacobi = new Jacobi(this);

            return jacobi.ValeursPropres();
        }


        /*Retourne la somme des elements autres que la diagonale*/
        public double SommeElementExtraDiagonaux()
        {
            double somme = 0.0d;
            for (int i = 0; i < this.dimension; i++)
            {
                for (int j = 0; j < this.dimension; j++)
                {
                    if (i != j)
                    {
                        somme += this.coefficient[i, j];
                    }
                }
            }
            return somme;
        }

    }

}
