﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaosTheoryProject.Core
{
    public class RungeKutta4
    {
        private int count;
        private double[] x;
        private double[] y;
        private double[] z;
        private double step;        

        public RungeKutta4(int count)
        {
            this.count = count;
            this.step = 0.01f;

            x = new double[count];
            y = new double[count];
            z = new double[count];
        }

        public void Compute()
        {
            x[0] = 0; y[0] = 1; z[0] = 0;

            double h = step;
            double[] k11 = new double[count];
            double[] k12 = new double[count];
            double[] k13 = new double[count];

            double[] k21 = new double[count];
            double[] k22 = new double[count];
            double[] k23 = new double[count];

            double[] k31 = new double[count];
            double[] k32 = new double[count];
            double[] k33 = new double[count];

            double[] k41 = new double[count];
            double[] k42 = new double[count];
            double[] k43 = new double[count];

            for (int i = 1; i < count; i++)
            {
                k11[i] = h * f1(x[i - 1], y[i - 1], z[i - 1]);
                k12[i] = h * f2(x[i - 1], y[i - 1], z[i - 1]);
                k13[i] = h * f3(x[i - 1], y[i - 1], z[i - 1]);

                k21[i] = h * f1(x[i - 1] + 0.5f * k11[i - 1], y[i - 1] + 0.5f * k12[i - 1], z[i - 1] + 0.5f * k13[i - 1]);
                k22[i] = h * f2(x[i - 1] + 0.5f * k11[i - 1], y[i - 1] + 0.5f * k12[i - 1], z[i - 1] + 0.5f * k13[i - 1]);
                k23[i] = h * f3(x[i - 1] + 0.5f * k11[i - 1], y[i - 1] + 0.5f * k12[i - 1], z[i - 1] + 0.5f * k13[i - 1]);

                k31[i] = h * f1(x[i - 1] + 0.5f * k21[i - 1], y[i - 1] + 0.5f * k22[i - 1], z[i - 1] + 0.5f * k23[i - 1]);
                k32[i] = h * f2(x[i - 1] + 0.5f * k21[i - 1], y[i - 1] + 0.5f * k22[i - 1], z[i - 1] + 0.5f * k23[i - 1]);
                k33[i] = h * f3(x[i - 1] + 0.5f * k21[i - 1], y[i - 1] + 0.5f * k22[i - 1], z[i - 1] + 0.5f * k23[i - 1]);

                k41[i] = h * f1(x[i - 1] + k31[i - 1], y[i - 1] + k32[i - 1], z[i - 1] + k33[i - 1]);
                k42[i] = h * f2(x[i - 1] + k31[i - 1], y[i - 1] + k32[i - 1], z[i - 1] + k33[i - 1]);
                k43[i] = h * f3(x[i - 1] + k31[i - 1], y[i - 1] + k32[i - 1], z[i - 1] + k33[i - 1]);

                x[i] = x[i - 1] + (k11[i] + 2 * k21[i] + 2 * k31[i] + k41[i]) / 6;
                y[i] = y[i - 1] + (k12[i] + 2 * k22[i] + 2 * k32[i] + k42[i]) / 6;
                z[i] = z[i - 1] + (k13[i] + 2 * k23[i] + 2 * k33[i] + k43[i]) / 6;
            }
        }

        private double f1(double x, double y, double z)
        {
            return -10 * x + 10 * y;
        }

        private double f2(double x, double y, double z)
        {
            return -x * z + 28 * x - y;
        }

        private double f3(double x, double y, double z)
        {
            return x * y - (8 / 3) * z;
        }

        public double GetX(int i)
        {
            if (i >= count || i < 0) return 0f;
            return x[i];
        }

        public double GetY(int i)
        {
            if (i >= count || i < 0) return 0f;
            return y[i];
        }

        public double GetZ(int i)
        {
            if (i >= count || i < 0) return 0f;
            return z[i];
        }

        public double[] GetXArray()
        {
            return x;
        }

        public double[] GetYArray()
        {
            return y;
        }

        public double[] GetZArray()
        {
            return z;
        }
    }
}
