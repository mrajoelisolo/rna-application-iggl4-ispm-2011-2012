﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaosTheoryProject.Core
{
    public class RNA
    {
        /*Nombre de couche du réseaux*/
        private const int M = 3;

        /*Pas d'apprentissage*/
        private const double NANO = 0.1d;

        /*Nombre totale de prototye qu'on entre pour l'apprentissage*/
        private const int NOMBRE_PROTOTYPE = 10;

        #region Attribut

        /*Le nombre d'unité de la couche cachée et de la couche d'entrée*/
        private int NombreUniteEntree;
        private int NombreUniteCachee;

        /*Poids avec indice de couches*/
        private double[, ,] W;

        /*Sortie des Couches*/
        private double[,] V;

        /*Serie Temporelle*/
        private double[] S;

        /*Sortie Desirée*/
        private double SortieDesiree;
        /*Sortie RNA*/
        private double SortieRNA;

        /*Erreur Quadratique Normalisée*/
        private double NMSE;

        #endregion

        /*Constructeur*/
        public RNA(int nbreUnitEntree, int nbreUnitCachee, double[] serieTemporelle, int indicePrototype)
        {
            this.NombreUniteEntree = nbreUnitEntree;
            this.NombreUniteCachee = nbreUnitCachee;

            /*Dimmension de V*/
            if (this.NombreUniteEntree > this.NombreUniteCachee)
            {
                this.V = new double[this.NombreUniteEntree + 1, M + 1];
                this.W = new double[this.NombreUniteEntree + 1, this.NombreUniteEntree + 1, M + 1];
            }
            else
            {
                this.V = new double[this.NombreUniteCachee + 1, M + 1];
                this.W = new double[this.NombreUniteCachee + 1, this.NombreUniteCachee + 1, M + 1];
            }

            /*Serie Temporelle*/
            this.S = new double[NOMBRE_PROTOTYPE + 1];

            for (int i = 1; i <= NOMBRE_PROTOTYPE; i++)
            {
                S[i] = UtilitaireNombre.Instance.FormatDouble(serieTemporelle[i - 1 + indicePrototype]);
            }

        }

        /*Fonction d'activation Sygmoïde*/
        private double g(double x)
        {
            return UtilitaireNombre.Instance.FormatDouble(1 / (1 + Math.Exp(-x)));
        }

        /*Fonction dérivée de la fonction d'activation g*/
        private double gPrim(double x)
        {
            return UtilitaireNombre.Instance.FormatDouble(Math.Exp(-x) / Math.Pow(1 + Math.Exp(-x), 2));
        }

        /*Calcul  de h(i,m)*/
        private double h(int i, int m)
        {
            double somme = 0.0d;
            int N = 0;

            if (m == 2)
            {
                N = this.NombreUniteEntree;
            }
            else
            {
                N = this.NombreUniteCachee;
            }

            for (int j = 1; j <= N; j++)
            {
                somme += UtilitaireNombre.Instance.FormatDouble(W[i, j, m]) * UtilitaireNombre.Instance.FormatDouble(V[j, m - 1]);
            }
            return UtilitaireNombre.Instance.FormatDouble(somme);
        }

        /*Etape 1 : Initialiser les poids à des petites valeurs aléatoires ici 0.1*/
        private void InitialiserPoids()
        {
            for (int i = 1; i <= this.NombreUniteCachee; i++)
            {
                W[1, i, 3] = 0.1;

                for (int j = 1; j <= this.NombreUniteEntree; j++)
                {
                    W[i, j, 2] = 0.1;
                }
            }
        }

        /*Etape 2 : Choisir un prototype */
        private void ChoisirPrototype(int indice)
        {
            int k = 1;
            int i;

            for (i = 1; i <= this.NombreUniteEntree; i++)
            {
                V[k, 1] = S[indice + i];
                k++;
            }

            SortieDesiree = S[i + indice];
        }

        /*Etape 3 : Propagation du signal vers l'avant*/
        private void Propager()
        {
            for (int m = 2; m < M; m++)
            {
                for (int i = 1; i <= this.NombreUniteCachee; i++)
                {
                    V[i, m] = UtilitaireNombre.Instance.FormatDouble(g(h(i, m)));
                }
            }
            /*Puisqu'il y a qu'une seule unité de sortie */
            V[1, M] = UtilitaireNombre.Instance.FormatDouble(g(h(1, M)));
            SortieRNA = V[1, M];
        }

        /*Etape 4 : Calcul de Delta pour la couche de Sortie*/
        private double CalculDelta()
        {
            return UtilitaireNombre.Instance.FormatDouble(gPrim(h(1, M) * (SortieDesiree - V[1, M])));
        }

        /*Etape 5 : Calcul des autres Delta*/
        private double[,] CalculAutreDelta()
        {
            double[,] delta = new double[this.NombreUniteCachee + 1, M];

            /*delta(1,3)*/
            double delta13 = CalculDelta();

            for (int m = M; m >= 3; m--)
            {
                for (int i = 1; i <= this.NombreUniteCachee; i++)
                {
                    /*Il n'y a qu'une seule unitée de sortie donc j=1 */
                    delta[i, m - 1] = UtilitaireNombre.Instance.FormatDouble(gPrim(h(i, m - 1) * (W[1, i, m] * delta13)));
                }
            }

            return delta;

        }

        /*Etape 6 : Mis a jour des connexion*/
        private void MisAjourConnexion()
        {
            double[, ,] DeltaW = new double[this.NombreUniteCachee + 1, this.NombreUniteEntree + 1, M + 1];

            double delta13 = CalculDelta();
            double[,] delta = CalculAutreDelta();

            /*Calcul des Grand Delta*/
            for (int m = M; m >= 2; m--)
            {
                for (int i = 1; i <= this.NombreUniteCachee; i++)
                {
                    for (int j = 1; j <= this.NombreUniteEntree; j++)
                    {
                        if (m == 3)
                        {
                            DeltaW[i, j, m] = UtilitaireNombre.Instance.FormatDouble(NANO * delta13 * V[j, m - 1]);
                        }
                        else
                        {
                            DeltaW[i, j, m] = UtilitaireNombre.Instance.FormatDouble(NANO * delta[i, m] * V[j, m - 1]);
                        }
                    }
                }
            }

            /*Mis à jour des poids WijNew = WijOld + DeltaWij*/
            for (int m = M; m >= 2; m--)
            {
                for (int i = 1; i <= this.NombreUniteCachee; i++)
                {
                    for (int j = 1; j <= this.NombreUniteEntree; j++)
                    {
                        W[i, j, m] = UtilitaireNombre.Instance.FormatDouble(W[i, j, m] + DeltaW[i, j, m]);
                    }
                }
            }

        }

        private double CalculVariance()
        {
            /*Moyenne*/
            double moyenne = 0.0d;
            for (int i = 1; i < S.Length; i++)
            {
                moyenne += S[i];
            }
            moyenne = UtilitaireNombre.Instance.FormatDouble(moyenne / S.Length);

            /*Calcul Variance*/
            double variance = 0.0d;
            for (int i = 1; i < S.Length; i++)
            {
                variance += UtilitaireNombre.Instance.FormatDouble(Math.Pow(S[i], 2));
            }

            variance = UtilitaireNombre.Instance.FormatDouble((variance - Math.Pow(moyenne, 2)) / S.Length);

            return variance;

        }

        /*Apprentissage*/
        public void Apprentissage()
        {
            double[] X = new double[NOMBRE_PROTOTYPE + 1];
            double[] XChapeau = new double[NOMBRE_PROTOTYPE + 1];

            InitialiserPoids();

            for (int i = 1; i < NOMBRE_PROTOTYPE - this.NombreUniteEntree; i++)
            {
                ChoisirPrototype(i);
                Propager();
                MisAjourConnexion();

                X[i] = SortieDesiree;
                XChapeau[i] = SortieRNA;
            }

            /*Calcul NMSE*/
            double ecart = 0.0d;

            for (int k = 1; k <= NOMBRE_PROTOTYPE; k++)
            {
                ecart += UtilitaireNombre.Instance.FormatDouble(Math.Pow(X[k] - XChapeau[k], 2));
            }

            NMSE = UtilitaireNombre.Instance.FormatDouble(ecart / (NOMBRE_PROTOTYPE * CalculVariance()));
        }

        /*Réinitialisation*/
        public void ReInit(int nbreUnitEntree, int nbreUnitCachee, double[] serieTemporelle, int indicePrototype)
        {
            this.NombreUniteEntree = nbreUnitEntree;
            this.NombreUniteCachee = nbreUnitCachee;

            /*Serie Temporelle*/
            this.S = new double[NOMBRE_PROTOTYPE + 1];

            for (int i = 1; i <= NOMBRE_PROTOTYPE; i++)
            {
                S[i] = UtilitaireNombre.Instance.FormatDouble(serieTemporelle[i - 1 + indicePrototype]);
            }
        }

        /*Fonction retournant l'erreur quadratique */
        public double GetNMSE()
        {
            return NMSE;
        }

        /* Getter et Setter W*/
        public double[, ,] GetW()
        {
            double[, ,] Result;
            if (this.NombreUniteEntree > this.NombreUniteCachee)
            {
                Result = new double[this.NombreUniteEntree + 1, this.NombreUniteEntree + 1, M + 1];
            }
            else
            {
                Result = new double[this.NombreUniteCachee + 1, this.NombreUniteCachee + 1, M + 1];
            }

            for (int i = 1; i <= this.NombreUniteCachee; i++)
            {
                Result[1, i, 3] = W[1, i, 3];

                for (int j = 1; j <= this.NombreUniteEntree; j++)
                {
                    Result[i, j, 2] = W[i, j, 2];
                }
            }

            return Result;
        }

        public void SetW(double[, ,] Valeur)
        {
            for (int i = 1; i <= this.NombreUniteCachee; i++)
            {
                W[1, i, 3] = Valeur[1, i, 3];

                for (int j = 1; j <= this.NombreUniteEntree; j++)
                {
                    W[i, j, 2] = Valeur[i, j, 2];
                }
            }
        }

        public int GetNombreUniteCachee()
        {
            return this.NombreUniteCachee;
        }

        public int GetNombreUniteEntree()
        {
            return this.NombreUniteEntree;
        }
    }
}
