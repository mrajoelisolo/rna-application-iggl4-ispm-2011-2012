﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaosTheoryProject.Core
{
    public class UtilitaireNombre
    {
        private static UtilitaireNombre m_Istance = new UtilitaireNombre();

        private UtilitaireNombre() { }

        public static UtilitaireNombre Instance
        {
            get
            {
                return m_Istance;
            }
        }

        public double FormatDouble(double valeur)
        {
            /*Les Résulats numériques avec 8 chiffres après la virgule*/
            string s_valeur = valeur.ToString("F8");

            return Double.Parse(s_valeur);
        }
    }
}
