﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ChaosTheoryProject.Core
{
    public class Prediction
    {
        /*Nombre de couche du réseaux*/
        private const int M = 3;

        /*Nombre totale de prototye qu'on entre pour l'apprentissage*/
        private const int NOMBRE_PROTOTYPE = 10;

        #region Attribut

        /*Le nombre d'unité de la couche cachée et de la couche d'entrée*/
        private int NombreUniteEntree;
        private int NombreUniteCachee;

        /*Poids avec indice de couches*/
        private double[, ,] W;

        /*Sortie des Couches*/
        private double[,] V;

        /*Serie Temporelle*/
        private double[] S;

        /*Sortie Desirée*/
        private double SortieDesiree;

        /*Sortie RNA*/
        private double SortieRNA;

        private double[] Ecart;

        private double[] XChapeau;

        private double[] X;

        private double[] YChapeau;

        #endregion

        public Prediction(int nbreUnitEntree, int nbreUnitCachee, double[] serieTemporelle, int indicePrototype, double[, ,] poids)
        {
            this.NombreUniteEntree = nbreUnitEntree;
            this.NombreUniteCachee = nbreUnitCachee;

            /*Dimmension de V*/
            if (this.NombreUniteEntree > this.NombreUniteCachee)
            {
                this.V = new double[this.NombreUniteEntree + 1, M + 1];
                this.W = new double[this.NombreUniteEntree + 1, this.NombreUniteEntree + 1, M + 1];
            }
            else
            {
                this.V = new double[this.NombreUniteCachee + 1, M + 1];
                this.W = new double[this.NombreUniteCachee + 1, this.NombreUniteCachee + 1, M + 1];
            }

            /*Affectation des poids*/
            for (int i = 1; i <= this.NombreUniteCachee; i++)
            {
                W[1, i, 3] = poids[1, i, 3];

                for (int j = 1; j <= this.NombreUniteEntree; j++)
                {
                    W[i, j, 2] = poids[i, j, 2];
                }
            }

            /*Serie Temporelle*/
            this.S = new double[serieTemporelle.Length];

            for (int i = 1; i < serieTemporelle.Length; i++)
            {
                S[i] = UtilitaireNombre.Instance.FormatDouble(serieTemporelle[i - 1 + indicePrototype]);
            }

        }

        /*Choisir un prototype */
        private void ChoixPrototypeUnPas(int indice)
        {
            int k = 1;
            int i;

            for (i = 1; i <= this.NombreUniteEntree; i++)
            {
                V[k, 1] = S[indice + i];
                k++;
            }

            SortieDesiree = S[i + indice];
        }

        /*Propagation du signal vers l'avant*/
        private void Propager()
        {
            for (int m = 2; m < M; m++)
            {
                for (int i = 1; i <= this.NombreUniteCachee; i++)
                {
                    V[i, m] = UtilitaireNombre.Instance.FormatDouble(g(h(i, m)));
                }
            }
            /*Puisqu'il y a qu'une seule unité de sortie */
            V[1, M] = UtilitaireNombre.Instance.FormatDouble(g(h(1, M)));
            SortieRNA = V[1, M];
        }

        /*Fonction d'activation Sygmoïde*/
        private double g(double x)
        {
            return UtilitaireNombre.Instance.FormatDouble(1 / (1 + Math.Exp(-x)));
        }

        /*Calcul  de h(i,m)*/
        private double h(int i, int m)
        {
            double somme = 0.0d;
            int N = 0;

            if (m == 2)
            {
                N = this.NombreUniteEntree;
            }
            else
            {
                N = this.NombreUniteCachee;
            }

            for (int j = 1; j <= N; j++)
            {
                somme += UtilitaireNombre.Instance.FormatDouble(W[i, j, m]) * UtilitaireNombre.Instance.FormatDouble(V[j, m - 1]);
            }
            return UtilitaireNombre.Instance.FormatDouble(somme);
        }

        public void PredireUnPas(int nbreValeurPredite)
        {
            X = new double[nbreValeurPredite + 1];
            XChapeau = new double[nbreValeurPredite + 1];
            Ecart = new double[nbreValeurPredite + 1];

            int k = 1;

            for (int i = 0; i < nbreValeurPredite; i++)
            {
                ChoixPrototypeUnPas(i);
                Propager();

                X[k] = SortieDesiree;
                XChapeau[k] = SortieRNA;

                Ecart[k] = UtilitaireNombre.Instance.FormatDouble(X[k] - XChapeau[k]);

                k++;
            }
        }

        private void ChoixPrototypePlusieursPas(double Sortie, int indice)
        {
            int k = 2;

            V[1, 1] = Sortie;

            for (int i = 0; i < this.NombreUniteEntree - 1; i++)
            {
                V[k, 1] = S[indice + i];
                k++;
            }
        }

        public void PredirePlusieursPas(int nbrePas)
        {
            YChapeau = new double[nbrePas + 1];

            int k = 1;

            ChoixPrototypeUnPas(0);
            Propager();

            YChapeau[k] = SortieRNA;

            for (int i = 1; i < nbrePas; i++)
            {
                ChoixPrototypePlusieursPas(YChapeau[k], i);
                Propager();

                k++;
                YChapeau[k] = SortieRNA;
            }
        }

        public double[] GetX()
        {
            return X;
        }

        public double[] GetXChapeau()
        {
            return XChapeau;
        }

        public double[] GetEcart()
        {
            return Ecart;
        }

        public double[] GetYChapeau()
        {
            return YChapeau;
        }

        public double[] GetS()
        {
            return S;
        }


    }
}
