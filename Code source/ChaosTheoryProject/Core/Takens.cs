﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaosTheoryProject.Core
{
    public class Takens
    {
        /*Constante Parametre de delai Taux*/
        private const int TAUX = 1;

        /*la série temporelle U*/
        private double[] U;

        /*Le vecteur Xbar*/
        private double[] Xbar;

        /*Valeur Propre*/
        private double[] valeursPropres;

        /*Erreur Approximation Moyenne*/
        private double[] epsilon;

        /*La matrice de covariance n*n */
        private Matrix matriceDeCovariance;

        /*Constructeur*/
        public Takens(double[] S, int n)
        {
            this.U = new double[n];

            this.Xbar = new double[n];

            this.matriceDeCovariance = new Matrix(n);

            /*Affetation des valeurs de S dans notre attributs*/
            for (int i = 0; i < n; i++)
            {
                U[i] = UtilitaireNombre.Instance.FormatDouble(S[i]);
            }

            epsilon = new double[n];
        }


        /************************************************************************
         *                                                                      *
         *                                                                      *
         *                      Methodes                                        *                                                                      *
         *                                                                      *
         * **********************************************************************/


        /*Construction de la séquence de vecteur à partir de la suite temporelle U*/
        private void createXbar()
        {
            for (int i = 0; i < U.Length; i++)
            {
                Xbar[i] = U[(i * TAUX)];
            }
        }

        /*definir la matrice de covariance*/
        private void defineMatriceCovariance()
        {
            for (int i = 0; i < Xbar.Length; i++)
            {
                for (int j = 0; j < Xbar.Length; j++)
                {
                    matriceDeCovariance[i, j] = UtilitaireNombre.Instance.FormatDouble(Xbar[i] * Xbar[j]);
                }
            }
        }

        /*Calcul des valeurs propres et rangé par ordre décroissant de la matrice de covariance*/
        private double[] calculValeursPropresMatriceCovariance()
        {
            return matriceDeCovariance.GetValeursPropres();
        }

        /*Obtention des erreurs d'approximation moyenne*/
        private double[] erreursApproximationMoyenne(double[] valeursPropres)
        {
            double[] eps = new double[valeursPropres.Length];

            for (int l = 0; l < valeursPropres.Length - 1; l++)
            {
                if (valeursPropres[l + 1] >= 0)
                {
                    eps[l] = Math.Sqrt(valeursPropres[l + 1]);
                }
                else
                {
                    eps[l] = 0;
                }
            }

            return eps;
        }

        /*obtention du nombre de couche d'entrée */
        public int GetNombreUniteCoucheEntree()
        {
            /*Construire une séquence de vecteur à partir de la série temorelle*/
            createXbar();

            /*Definir la matrice de covariance*/
            defineMatriceCovariance();

            /*Calcul des valeurs propres que lon range par ordre décroissant*/
            valeursPropres = calculValeursPropresMatriceCovariance();

            /*Erreurs d'approximation moyennes*/
            epsilon = erreursApproximationMoyenne(valeursPropres);

            /*La premiere valeur de l correspondant au premier plateau 
             * càd epsilonne[l]=epsilonne[l+1] est égale au nombre d'unité d'entrée du RNA*/
            int nbreUniteEntree = 0;

            for (int i = 0; i < epsilon.Length; i++)
            {
                if (epsilon[i] == epsilon[i + 1])
                {
                    /*indice du tableau commence par 0 donc indice=i+1*/
                    nbreUniteEntree = i + 1;
                    break;
                }
            }

            return nbreUniteEntree;
        }

        public double[] GetValeursPropres()
        {
            return valeursPropres;
        }

        public double[] GetEpsilonne()
        {
            return epsilon;
        }
    }
}
