﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaosTheoryProject.Core
{
    public class TVector
    {
        private double[] m_Values;

        public TVector(int n)
        {
            m_Values = new double[n];
        }

        public double GetValueAt(int i)
        {
            return m_Values[i];
        }

        public void SetValueAt(int i, double value)
        {
            m_Values[i] = value;
        }

        public int Size
        {
            get
            {
                return m_Values.Length;
            }
        }
    }
}
