﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ChaosTheoryProject.Drawing.Shapes;
using ChaosTheoryProject.Core;

namespace ChaosTheoryProject
{
    public partial class FormArchitecture : Form
    {
        private Form m_Parent;
        private double[] m_X;
        private int m_Entries;
        private int m_Hidden;
        private bool m_CanContinue = false;

        public FormArchitecture(Form parent, double[] X)
        {
            InitializeComponent();
            CenterToScreen();

            m_Parent = parent;
            m_X = X;
        }

        private void FormArchitecture_FormClosed(object sender, FormClosedEventArgs e)
        {
            m_Parent.Show();
        }

        private void btnGenerateRNA_Click(object sender, EventArgs e)
        {
            m_Entries = ArchitectureRNA.Instance.GetNombreUniteEntree(m_X);
            m_Hidden = ArchitectureRNA.Instance.GetNombreUniteCachee(m_X);
            int output = 1;

            TRna rna = new TRna(m_Entries, m_Hidden, output);
            rna.SetBounds(50, 10, 640, 480);
            tCanvas1.ClearShapes();
            tCanvas1.AddShape(rna);

            m_CanContinue = true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (!m_CanContinue)
            {
                MessageBox.Show("Impossible de continuer sans déterminer l'architecture optimale");
                return;
            }

            FormApprentissage f = new FormApprentissage(this, m_Entries, m_Hidden, m_X);
            f.Show();
            this.Hide();
        }
    }
}
