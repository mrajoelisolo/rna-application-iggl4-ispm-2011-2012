﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ChaosTheoryProject.Drawing.Shapes;
using ChaosTheoryProject.Core;

namespace ChaosTheoryProject
{
    public partial class FormMain : Form
    {
        private TCustomGraph m_Graph1;

        public FormMain()
        {
            InitializeComponent();
            CenterToScreen();

            m_Graph1 = new TCustomGraph(tCanvas1);
            tCanvas1.AddShape(m_Graph1);

            seriesBindingSource.DataSource = m_Graph1.GetListSerie();            
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            double[] X = m_Graph1.GetArraySerie();
            FormArchitecture f = new FormArchitecture(this, X);
            f.Show();
            this.Hide();
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            
        }
    }
}
