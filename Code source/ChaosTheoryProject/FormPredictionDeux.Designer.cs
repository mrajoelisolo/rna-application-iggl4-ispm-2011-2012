﻿namespace ChaosTheoryProject
{
    partial class FormPredictionDeux
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPredictionDeux));
            this.resultBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cbPas = new System.Windows.Forms.ComboBox();
            this.btnPredict = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnNext = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.deltaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resultDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.excpectedResultDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.secondEntryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.firstEntryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iterationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtgPredictionUnPas = new System.Windows.Forms.DataGridView();
            this.GraphPrediction = new ZedGraph.ZedGraphControl();
            ((System.ComponentModel.ISupportInitialize)(this.resultBindingSource)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPredictionUnPas)).BeginInit();
            this.SuspendLayout();
            // 
            // resultBindingSource
            // 
            this.resultBindingSource.DataSource = typeof(ChaosTheoryProject.Datasource.RPredictionResult);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 292F));
            this.tableLayoutPanel2.Controls.Add(this.btnPredict, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.cbPas, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 53);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(786, 44);
            this.tableLayoutPanel2.TabIndex = 10;
            // 
            // cbPas
            // 
            this.cbPas.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbPas.FormattingEnabled = true;
            this.cbPas.Items.AddRange(new object[] {
            "3",
            "10",
            "20"});
            this.cbPas.Location = new System.Drawing.Point(497, 11);
            this.cbPas.Name = "cbPas";
            this.cbPas.Size = new System.Drawing.Size(121, 21);
            this.cbPas.TabIndex = 3;
            this.cbPas.Text = "3";
            // 
            // btnPredict
            // 
            this.btnPredict.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnPredict.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPredict.Location = new System.Drawing.Point(3, 8);
            this.btnPredict.Name = "btnPredict";
            this.btnPredict.Size = new System.Drawing.Size(300, 28);
            this.btnPredict.TabIndex = 2;
            this.btnPredict.Text = "Calculer";
            this.btnPredict.UseVisualStyleBackColor = true;
            this.btnPredict.Click += new System.EventHandler(this.btnPredict_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.lblTitle, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(786, 44);
            this.tableLayoutPanel3.TabIndex = 7;
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(3, 7);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(428, 29);
            this.lblTitle.TabIndex = 6;
            this.lblTitle.Text = "Prédiction à plusieurs pas en avant";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.flowLayoutPanel1.Controls.Add(this.btnNext);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 535);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(786, 34);
            this.flowLayoutPanel1.TabIndex = 8;
            // 
            // btnNext
            // 
            this.btnNext.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Location = new System.Drawing.Point(683, 3);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(100, 30);
            this.btnNext.TabIndex = 5;
            this.btnNext.Text = "Suivant";
            this.btnNext.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dtgPredictionUnPas, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.GraphPrediction, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(792, 572);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // deltaDataGridViewTextBoxColumn
            // 
            this.deltaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.deltaDataGridViewTextBoxColumn.DataPropertyName = "Delta";
            this.deltaDataGridViewTextBoxColumn.HeaderText = "Ecart";
            this.deltaDataGridViewTextBoxColumn.Name = "deltaDataGridViewTextBoxColumn";
            this.deltaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // resultDataGridViewTextBoxColumn
            // 
            this.resultDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.resultDataGridViewTextBoxColumn.DataPropertyName = "Result";
            this.resultDataGridViewTextBoxColumn.HeaderText = "Sortié donnée par le réseau";
            this.resultDataGridViewTextBoxColumn.Name = "resultDataGridViewTextBoxColumn";
            this.resultDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // excpectedResultDataGridViewTextBoxColumn
            // 
            this.excpectedResultDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.excpectedResultDataGridViewTextBoxColumn.DataPropertyName = "ExcpectedResult";
            this.excpectedResultDataGridViewTextBoxColumn.HeaderText = "Valeur attendue";
            this.excpectedResultDataGridViewTextBoxColumn.Name = "excpectedResultDataGridViewTextBoxColumn";
            this.excpectedResultDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // secondEntryDataGridViewTextBoxColumn
            // 
            this.secondEntryDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.secondEntryDataGridViewTextBoxColumn.DataPropertyName = "SecondEntry";
            this.secondEntryDataGridViewTextBoxColumn.HeaderText = "Entrée 2";
            this.secondEntryDataGridViewTextBoxColumn.Name = "secondEntryDataGridViewTextBoxColumn";
            this.secondEntryDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // firstEntryDataGridViewTextBoxColumn
            // 
            this.firstEntryDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.firstEntryDataGridViewTextBoxColumn.DataPropertyName = "FirstEntry";
            this.firstEntryDataGridViewTextBoxColumn.HeaderText = "Entrée 1";
            this.firstEntryDataGridViewTextBoxColumn.Name = "firstEntryDataGridViewTextBoxColumn";
            this.firstEntryDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iterationDataGridViewTextBoxColumn
            // 
            this.iterationDataGridViewTextBoxColumn.DataPropertyName = "Iteration";
            this.iterationDataGridViewTextBoxColumn.HeaderText = "Iteration";
            this.iterationDataGridViewTextBoxColumn.Name = "iterationDataGridViewTextBoxColumn";
            this.iterationDataGridViewTextBoxColumn.ReadOnly = true;
            this.iterationDataGridViewTextBoxColumn.Width = 50;
            // 
            // dtgPredictionUnPas
            // 
            this.dtgPredictionUnPas.AllowUserToAddRows = false;
            this.dtgPredictionUnPas.AllowUserToDeleteRows = false;
            this.dtgPredictionUnPas.AutoGenerateColumns = false;
            this.dtgPredictionUnPas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPredictionUnPas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iterationDataGridViewTextBoxColumn,
            this.firstEntryDataGridViewTextBoxColumn,
            this.secondEntryDataGridViewTextBoxColumn,
            this.excpectedResultDataGridViewTextBoxColumn,
            this.resultDataGridViewTextBoxColumn,
            this.deltaDataGridViewTextBoxColumn});
            this.dtgPredictionUnPas.DataSource = this.resultBindingSource;
            this.dtgPredictionUnPas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPredictionUnPas.Location = new System.Drawing.Point(3, 403);
            this.dtgPredictionUnPas.Name = "dtgPredictionUnPas";
            this.dtgPredictionUnPas.ReadOnly = true;
            this.dtgPredictionUnPas.RowHeadersVisible = false;
            this.dtgPredictionUnPas.RowTemplate.Height = 23;
            this.dtgPredictionUnPas.Size = new System.Drawing.Size(786, 126);
            this.dtgPredictionUnPas.TabIndex = 9;
            // 
            // GraphPrediction
            // 
            this.GraphPrediction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GraphPrediction.Location = new System.Drawing.Point(3, 103);
            this.GraphPrediction.Name = "GraphPrediction";
            this.GraphPrediction.ScrollGrace = 0D;
            this.GraphPrediction.ScrollMaxX = 0D;
            this.GraphPrediction.ScrollMaxY = 0D;
            this.GraphPrediction.ScrollMaxY2 = 0D;
            this.GraphPrediction.ScrollMinX = 0D;
            this.GraphPrediction.ScrollMinY = 0D;
            this.GraphPrediction.ScrollMinY2 = 0D;
            this.GraphPrediction.Size = new System.Drawing.Size(786, 294);
            this.GraphPrediction.TabIndex = 11;
            // 
            // FormPredictionDeux
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(792, 572);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormPredictionDeux";
            this.Text = "RNA IGGL4 - Prédiction à plusieurs pas en avant";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormPredictionDeux_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.resultBindingSource)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPredictionUnPas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource resultBindingSource;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnPredict;
        private System.Windows.Forms.ComboBox cbPas;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView dtgPredictionUnPas;
        private System.Windows.Forms.DataGridViewTextBoxColumn iterationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstEntryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn secondEntryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn excpectedResultDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn resultDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deltaDataGridViewTextBoxColumn;
        private ZedGraph.ZedGraphControl GraphPrediction;
    }
}