﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ChaosTheoryProject.Core;
using ChaosTheoryProject.Datasource;
using ZedGraph;

namespace ChaosTheoryProject
{
    public partial class FormPredictionUn : Form
    {
        private Form m_Parent;
        private int m_Entries;
        private int m_Hidden;
        private double[] m_X;

        public FormPredictionUn(Form parent, int entries, int hidden, double[] X)
        {
            InitializeComponent();
            CenterToScreen();

            m_Parent = parent;
            m_Entries = entries;
            m_Hidden = hidden;
            m_X = X;
        }

        private void btnPredict_Click(object sender, EventArgs e)
        {
            int nombreUniteEntree = m_Entries;
            int nombreUniteCachee = m_Hidden;


            double[, ,] Poids = Apprentissage.Instance.Reseau.GetW();

            Prediction predicteur = new Prediction(nombreUniteEntree, nombreUniteCachee, m_X, 0, Poids);

            predicteur.PredireUnPas(10);

            Afficher(predicteur);
            AfficheGraphe();
        }

        public void Afficher(Prediction P)
        {            
            double[] X = P.GetX();
            double[] XChapeau = P.GetXChapeau();
            double[] Serie = P.GetS();

            resultBindingSource.Clear();
            for (int i = 1; i < X.Length; i++)
            {
                RPredictionResult prediction = new RPredictionResult();
                prediction.Iteration = i;
                prediction.FirstEntry = Serie[i];
                prediction.SecondEntry = Serie[i + 1];
                prediction.ExcpectedResult = Serie[i + 3];
                prediction.Result = XChapeau[i];

                resultBindingSource.Add(prediction);
            }
        }

        private void AfficheGraphe()
        {

            GraphPane myPane = GraphPrediction.GraphPane;

            myPane.Title.Text = "Graphe de prédiction à un pas en avant";
            myPane.XAxis.Title.Text = "Itération";
            myPane.YAxis.Title.Text = "Valeur Attendue";
            myPane.Y2Axis.Title.Text = "Valeur prédite";

            PointPairList list = new PointPairList();
            PointPairList list2 = new PointPairList();


            for (int i = 0; i < dtgPredictionUnPas.RowCount - 1; i++)
            {
                list.Add(i, Double.Parse(dtgPredictionUnPas.Rows[i].Cells[3].Value.ToString()));
                list2.Add(i, Double.Parse(dtgPredictionUnPas.Rows[i].Cells[4].Value.ToString()));
            }

            LineItem myCurve = myPane.AddCurve("Valeur Attendue",
                list, Color.Blue, SymbolType.Star);

            myCurve.Symbol.Fill = new Fill(Color.White);

            myCurve = myPane.AddCurve("Valeur prédite",
                list2, Color.Green, SymbolType.XCross);

            myCurve.Symbol.Fill = new Fill(Color.White);

            myCurve.IsY2Axis = true;

            myPane.XAxis.MajorGrid.IsVisible = true;

            myPane.YAxis.Scale.FontSpec.FontColor = Color.Blue;
            myPane.YAxis.Title.FontSpec.FontColor = Color.Blue;

            myPane.YAxis.MajorTic.IsOpposite = false;
            myPane.YAxis.MinorTic.IsOpposite = false;

            myPane.YAxis.MajorGrid.IsZeroLine = false;

            myPane.YAxis.Scale.Align = AlignP.Inside;

            myPane.Y2Axis.IsVisible = true;

            myPane.Y2Axis.Scale.FontSpec.FontColor = Color.Green;
            myPane.Y2Axis.Title.FontSpec.FontColor = Color.Green;

            myPane.Y2Axis.MajorTic.IsOpposite = false;
            myPane.Y2Axis.MinorTic.IsOpposite = false;

            myPane.Y2Axis.MajorGrid.IsVisible = true;

            myPane.Y2Axis.Scale.Align = AlignP.Inside;

            myPane.Chart.Fill = new Fill(Color.White, Color.LightGray, 45.0f);

            GraphPrediction.IsShowHScrollBar = true;
            GraphPrediction.IsShowVScrollBar = true;
            GraphPrediction.IsAutoScrollRange = true;
            GraphPrediction.IsScrollY2 = true;

            GraphPrediction.AxisChange();

            GraphPrediction.Invalidate();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {            
            FormPredictionDeux f = new FormPredictionDeux(this, m_Entries, m_Hidden, m_X);
            f.Show();
            this.Hide();
        }

        private void FormPredictionUn_FormClosed(object sender, FormClosedEventArgs e)
        {
            m_Parent.Show();
        } 
    }
}
