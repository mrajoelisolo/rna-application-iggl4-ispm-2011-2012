﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaosTheoryProject.Datasource
{
    public class RPredictionResult
    {
        private int m_Iteration;
        public int Iteration
        {
            get
            {
                return m_Iteration;
            }

            set
            {
                m_Iteration = value;
            }
        }

        private double m_FirstEntry;
        public double FirstEntry
        {
            get
            {
                return m_FirstEntry;
            }

            set
            {
                m_FirstEntry = value;
            }
        }

        private double m_SecondEntry;
        public double SecondEntry
        {
            get
            {
                return m_SecondEntry;
            }

            set
            {
                m_SecondEntry = value;
            }
        }

        private double m_ExpectedResult;
        public double ExcpectedResult
        {
            get
            {
                return m_ExpectedResult;
            }

            set
            {
                m_ExpectedResult = value;
            }
        }

        private double m_Result;
        public double Result
        {
            get
            {
                return m_Result;
            }

            set
            {
                m_Result = value;
            }
        }

        public double Delta
        {
            get
            {
                return Result - ExcpectedResult;   
            }
        }
    }
}
