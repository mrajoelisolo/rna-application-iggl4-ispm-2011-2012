﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChaosTheoryProject.Datasource
{
    public class RSerie
    {
        public RSerie(int i, double value)
        {
            m_Index = i;
            m_XValue = value;
        }

        private int m_Index;
        public int Index
        {
            get
            {
                return m_Index;
            }

            set
            {
                m_Index = value;
            }
        }

        private double m_XValue;
        public double XValue
        {
            get
            {
                return m_XValue;
            }

            set
            {
                m_XValue = value;
            }
        }
    }
}
